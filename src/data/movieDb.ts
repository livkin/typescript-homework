import { HttpResponse } from '../adapters/httpResponse';
import { Page } from '../entities/page';
import { Movie } from '../entities/movie';

export class MovieDB {

  private key: string;
  private host: string;
  private version: string;

  constructor() {
    this.key = '96589c142ba72cafd0b6bf2fe20286a0';
    this.host = 'https://api.themoviedb.org';
    this.version = '3';
  }

  private getQueryURL(path: string, query: string = ''): string {
    return `${this.host}/${this.version}/${path}/?api_key=${this.key}${query}`;
  }

  private async fetchMovies<T>(url: string): Promise<HttpResponse<T>> {
    const responce: HttpResponse<T> = await fetch(url);
    try {
      responce.parsedBody = await responce.json();
    } catch (ex) { }
    if (!responce.ok) {
      throw new Error(responce.statusText);
    }
    return responce;
  }

  async getPage(url: string): Promise<Page> {
    const responce: HttpResponse<Page> = await this.fetchMovies(url);
    if (!responce.parsedBody) {
      return {
        page: 0,
        results: [],
        total_pages: 0,
        total_results: 0
      };
    }
    return responce.parsedBody;
  }

  async search(searchString: string, page: number = 0): Promise<Page> {
    // TODO ?shoud we parse searchString for inject
    const url = this.getQueryURL('search/movie', `&query=${searchString}${page === 0 ? '' : `&page=${page}`}`);

    return await this.getPage(url);

  }

  async popular(page: number = 0): Promise<Page> {

    const url = this.getQueryURL(`movie/popular`, page === 0 ? '' : `&page=${page}`);
    
    return await this.getPage(url);

    // console.log(url);

    // const responce: HttpResponse<Page> = await this.fetchMovies(url);

    // if (!responce.parsedBody) {
    //   return {
    //     page: 0,
    //     results: [],
    //     total_pages: 0,
    //     total_results: 0
    //   };
    // }
    // return responce.parsedBody;
  }

  async upcoming(page: number = 0): Promise<Page> {
    
    const url = this.getQueryURL(`movie/upcoming`, page === 0 ? '' : `&page=${page}`);
    
    return await this.getPage(url);

    // const responce: HttpResponse<Page> = await this.fetchMovies(url);

    // if (!responce.parsedBody) {
    //   return {
    //     page: 0,
    //     results: [],
    //     total_pages: 0,
    //     total_results: 0
    //   };
    // }
    // return responce.parsedBody;
  }

  async topRated(page: number = 0): Promise<Page> {
    const url = this.getQueryURL(`movie/top_rated`, page === 0 ? '' : `&page=${page}`);
    return await this.getPage(url);
    // const responce: HttpResponse<Page> = await this.fetchMovies(url);

    // if (!responce.parsedBody) {
    //   return {
    //     page: 0,
    //     results: [],
    //     total_pages: 0,
    //     total_results: 0
    //   };
    // }
    // return responce.parsedBody;
  }

}


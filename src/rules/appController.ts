import { MovieDB } from '../data/movieDb';
import { Movie } from '../entities/movie'
import { cardContainerController } from './cardContainer';

enum ApiQueries {
  Search,
  Popular,
  Upcoming,
  TopRated
}

export class AppController {

  private currentPageNumber: number;
  private lastApiQuery: ApiQueries;

  private movieDb: MovieDB;

  constructor() {
    this.currentPageNumber = 0;
    this.movieDb = new MovieDB();

  }


  //load-more
  private async loadMore() {

    console.log(this.lastApiQuery);

    if (this.lastApiQuery === ApiQueries.Popular) {

      const result = await this.movieDb.popular(this.currentPageNumber + 1);
      this.currentPageNumber = result.page;
      const cardContainer: cardContainerController = new cardContainerController();
      cardContainer.addCards(result.results);
      this.setRandomMovie(result.results);

    } else if (this.lastApiQuery === ApiQueries.TopRated) {

      const result = await this.movieDb.topRated(this.currentPageNumber + 1);
      this.currentPageNumber = result.page;
      const cardContainer: cardContainerController = new cardContainerController();
      cardContainer.addCards(result.results);
      this.setRandomMovie(result.results);

    } else if (this.lastApiQuery === ApiQueries.Upcoming) {

      const result = await this.movieDb.upcoming(this.currentPageNumber + 1);
      this.currentPageNumber = result.page;
      const cardContainer: cardContainerController = new cardContainerController();
      cardContainer.addCards(result.results);
      this.setRandomMovie(result.results);

    } else if (this.lastApiQuery === ApiQueries.Search) {

      const search = document.getElementById('search');

      if (search) {
        if (search.value) {
          const result = await this.movieDb.search(search.value, this.currentPageNumber + 1);
          this.currentPageNumber = result.page;
          const cardContainer: cardContainerController = new cardContainerController();
          cardContainer.addCards(result.results);

          if (result.results.length > 0) {
            this.setRandomMovie(result.results);
          }
        }
      }

    }

  }

  private setLoadMoreListener(): void {

    const button = document.getElementById('load-more');

    button?.addEventListener('click', async (e: Event) => {

      this.loadMore();

    });

  }

  private setRandomMovie(movies: Movie[]): void {

    const randMovie = movies[Math.floor(Math.random() * movies.length)];

    const elemRandomMovieName = document.getElementById('random-movie-name');
    const elemRandomMovieDescription = document.getElementById('random-movie-description');

    if (elemRandomMovieName) {
      elemRandomMovieName.innerText = randMovie.title;
    }

    if (elemRandomMovieDescription) {
      elemRandomMovieDescription.innerText = randMovie.overview;
    }


  }

  private async setPopular() {

    const result = await this.movieDb.popular();
    this.currentPageNumber = result.page;

    const cardContainer: cardContainerController = new cardContainerController();

    cardContainer.placeCards(result.results);

    this.lastApiQuery = ApiQueries.Popular;

    this.setRandomMovie(result.results);

  }

  private setPopularListener(): void {

    const button = document.getElementById('popular');

    button?.addEventListener('click', async (e: Event) => {

      this.setPopular();

    });

  }

  private async setSearch() {
    const search = document.getElementById('search');


    if (search) {
      if (search.value) {
        const result = await this.movieDb.search(search.value);
        this.currentPageNumber = result.page;
        const cardContainer: cardContainerController = new cardContainerController();
        cardContainer.placeCards(result.results);

        this.lastApiQuery = ApiQueries.Search;

        if (result.results.length > 0) {
          this.setRandomMovie(result.results);
        }
      }
    }

  }

  private setSearchListener(): void {

    const button = document.getElementById('submit');
    button?.addEventListener('click', async (e: Event) => {

      this.setSearch();

    });

  }

  private async setUpcoming() {

    const result = await this.movieDb.upcoming();
    this.currentPageNumber = result.page;
    const cardContainer: cardContainerController = new cardContainerController();
    cardContainer.placeCards(result.results);

    this.lastApiQuery = ApiQueries.Upcoming;

    this.setRandomMovie(result.results);

  }

  private setUpcomingListener(): void {

    const button = document.getElementById('upcoming');

    button?.addEventListener('click', async (e: Event) => {

      this.setUpcoming();

    });

  }

  private async setTopRated() {

    const result = await this.movieDb.topRated();
    this.currentPageNumber = result.page;
    const cardContainer: cardContainerController = new cardContainerController();
    cardContainer.placeCards(result.results);

    this.lastApiQuery = ApiQueries.TopRated;

    this.setRandomMovie(result.results);

  }

  private setTopRatedListener(): void {

    const button = document.getElementById('top_rated');

    button?.addEventListener('click', async (e: Event) => {

      this.setTopRated();

    });

  }

  defaultList() {
    this.setPopular();
  }

  setEventListeners(): void {

    this.setSearchListener();
    this.setPopularListener();
    this.setUpcomingListener();
    this.setTopRatedListener();
    this.setLoadMoreListener();

  }

}
import { MovieCard } from '../adapters/DOM/movieCard'
import { MovieDB } from '../data/movieDb'
import { Movie } from '../entities/movie';

export class cardContainerController {

  private cardContainer: HTMLElement | null;

  constructor() {
    this.cardContainer = document.getElementById('film-container');

    if (!this.cardContainer) {
      throw Error('can`t find element by id "film-container"');
    }
  }

  placeCards(movies: Movie[]) {
    const content: string = movies.reduce((acc, curr) => {
      
      const movieCard: MovieCard = new MovieCard(
        curr.id,
        'https://image.tmdb.org/t/p/w342' + curr.poster_path,
        false,
        curr.overview,
        curr.release_date
        );
      return acc += movieCard.getCard();
    
    }, '');

    if (this.cardContainer) {
      this.cardContainer.innerHTML = content;
    }
  
  }

  addCards(movies: Movie[]) {
    const content: string = movies.reduce((acc, curr) => {
      
      const movieCard: MovieCard = new MovieCard(
        curr.id,
        'https://image.tmdb.org/t/p/w342' + curr.poster_path,
        false,
        curr.overview,
        curr.release_date
        );
      return acc += movieCard.getCard();
    
    }, '');

    if (this.cardContainer) {
      this.cardContainer.innerHTML += content;
    }
  
  }

}
import { MovieDB } from './data/movieDb'
import { AppController } from './rules/appController';
import { cardContainerController } from './rules/cardContainer';

export async function render(): Promise<void> {
    // TODO render your app here

    const app = new AppController;
    app.setEventListeners();
    app.defaultList();

}

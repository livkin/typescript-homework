export class MovieCard {

  private id: string;
  private imgUrl: string;
  private liked: boolean;
  private cardText: string;
  private movieDate: string;

  constructor(id: string, imgUrl: string, liked: boolean, cardText: string, movieDate: string) {
  
    this.id = id;
    this.imgUrl = imgUrl;
    this.liked = this.isLiked();
    this.cardText = cardText;
    this.movieDate = movieDate;

  }

  private isLiked() {
    return window.localStorage.getItem(this.id) !== null;
  }

  getCard(): string {

    // const card = new HTMLDivElement;

    // simple soution as a proptotype
    //TODO create div properly 
    const innerHTML: string = `    
<div class="col-lg-3 col-md-4 col-12 p-2">
  <div class="card shadow-sm">
    <img src="${this.imgUrl}"/>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      stroke="red"
      fill="${this.liked ? 'red' : '#ff000078'}"
      width="50"
      height="50"
      class="bi bi-heart-fill position-absolute p-2"
      viewBox="0 -2 18 22"
      onclick = "window.localStorage.getItem(${this.id}) !== null ? window.localStorage.removeItem(${this.id}) : window.localStorage.setItem(${this.id}, 'liked');"  
    >
      <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
    </svg>
    <div class="card-body">
     <p class="card-text truncate">
        ${this.cardText}
      </p>
      <div
        class="
        d-flex
        justify-content-between
        align-items-center
        "
      >
        <small class="text-muted">${this.movieDate}</small>
    </div>
  </div>
</div>
</div>
`;

    return innerHTML;
  }
}